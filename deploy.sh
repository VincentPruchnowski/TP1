#!/bin/bash

# Fonction pour vérifier l'installation d'une commande
check_command() {
    command -v "$1" >/dev/null 2>&1 || { echo >&2 "$1 n'est pas installé. Veuillez l'installer."; exit 1; }
}

# Vérification de Terraform
check_command "terraform"

# Chemin du fichier Terraform
terraform_dir="./terraform"

# Initialisation de Terraform si nécessaire
if [ ! -d "$terraform_dir/.terraform" ]; then
    echo "Initialisation de Terraform..."
    terraform -chdir="$terraform_dir" init
fi

# Application de la création avec Terraform
echo "Création des ressources avec Terraform..."
terraform -chdir="$terraform_dir" apply -auto-approve

# Vérification de Ansible
check_command "ansible"

# Chemin du répertoire Ansible
ansible_dir="./ansible"

# Vérification de la présence du fichier hosts pour Ansible
if [ ! -f "$ansible_dir/hosts" ]; then
    echo "Création du fichier hosts pour Ansible..."
    echo "[wordpress]" > "$ansible_dir/hosts"
    echo "$(terraform -chdir="$terraform_dir" output wordpress_instance_ip)" >> "$ansible_dir/hosts"
fi

# Déploiement avec Ansible
echo "Déploiement avec Ansible..."
ansible-playbook -i "$ansible_dir/hosts" "$ansible_dir/wordpress_playbook.yml" --private-key="~/.ssh/id_rsa"

# Vérification de l'application
wordpress_ip=$(terraform -chdir="$terraform_dir" output wordpress_instance_ip)
echo "Vérification de l'application..."
curl_result=$(curl -s "$wordpress_ip" | grep "Installation de WordPress")
if [ -n "$curl_result" ]; then
    echo "Erreur : L'application ne fonctionne pas comme prévu."
else
    echo "L'application fonctionne correctement."
fi

# Déploiement avec Ansible pour MySQL
echo "Déploiement avec Ansible pour MySQL..."
ansible-playbook -i "$ansible_dir/hosts" "$ansible_dir/mysql_playbook.yml" --private-key="~/.ssh/id_rsa"