provider "google" {
  credentials = file(var.service_account_key_path)
  project     = var.project_id
  region      = var.region
}

resource "google_compute_network" "vpc_network" {
  name                    = "wordpress-vpc"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "vpc_subnet" {
  name          = "wordpress-subnet"
  ip_cidr_range = "10.0.1.0/24"
  network       = google_compute_network.vpc_network.name
}

resource "google_compute_firewall" "http_https" {
  name = "allow-http-https-ssh"
  allow {
    ports    = ["80","443","22"]
    protocol = "tcp"
  }
  direction     = "INGRESS"
  network       = google_compute_network.vpc_network.id
  priority      = 1000
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["wordpress"]
}

resource "google_compute_firewall" "mysql" {
  name = "allow-mysql"
  allow {
    ports    = ["3306","22"]
    protocol = "tcp"
  }
  direction     = "INGRESS"
  network       = google_compute_network.vpc_network.id
  priority      = 1000
  source_ranges = ["10.0.1.0/24"]
  target_tags   = ["wordpress"]

}

resource "google_compute_firewall" "icmp" {
  name        = "allow-icmp"
  allow {
    protocol = "icmp"
  }
  direction     = "INGRESS"
  network       = google_compute_network.vpc_network.id
  priority      = 1000
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["wordpress"]
}

resource "google_compute_instance" "wordpress_instance" {
  name         = "wordpress-instance"
  machine_type = "f1-micro"
  zone         = var.zone

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.vpc_subnet.name
    access_config {
      
    }
  }

  metadata = {
    ssh-keys = "ansible:${file(var.ssh_public_key_path)}"
  }

  tags = ["allow-http", "wordpress"]
}

resource "google_compute_instance" "db_instance" {
  name         = "db-instance"
  machine_type = "f1-micro"
  zone         = var.zone

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.vpc_subnet.name
    access_config {
      
    }
  }

  metadata = {
    ssh-keys = "ansible:${file(var.ssh_public_key_path)}"
  }

  tags = ["allow-http", "wordpress"]
}

output "wordpress_instance_ip" {
  value = length(google_compute_instance.wordpress_instance.network_interface[0].access_config) > 0 ? google_compute_instance.wordpress_instance.network_interface[0].access_config[0].nat_ip : null
}

output "db_instance_ip" {
  value = length(google_compute_instance.db_instance.network_interface[0].access_config) > 0 ? google_compute_instance.db_instance.network_interface[0].access_config[0].nat_ip : null
}

