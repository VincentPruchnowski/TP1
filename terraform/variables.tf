variable "project_id" {
  description = "ID du projet GCP"
}

variable "region" {
  default = "us-west1"
  description = "Région GCP"
}

variable "zone" {
  default = "us-west1-a"
  description = "Zone GCP"
}

variable "service_account_key_path" {
  description = "Chemin vers la clé du compte de service GCP"
}

variable "ssh_public_key_path" {
  description = "Chemin vers la clé publique SSH pour l'authentification"
}
