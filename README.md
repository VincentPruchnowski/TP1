# TP1


## Ojectif:
Automatiser le déploiement de ces sites Wordpress dans des VM sur Google Cloud Plateform.


### Description des fichiers fournis:
- Le dossier terraform contient les fichiers:
    main.tf: est généralement utilisé pour définir la configuration principale de notre infrastructure.
    terraform.tfvars: est utilisé pour définir les valeurs des variables utilisées dans notre configuration Terraform.
    variables.tf: est souvent utilisé pour déclarer et définir les variables que nous utilisons dans notre configuration Terraform.
- Le dossier ansible contient les fichiers:
    group_vars: est utilisé pour stocker des variables spécifiques à un groupe d'hôtes (hosts) dans notre infrastructure.
    hosts: le fichier hosts (ou parfois appelé fichier d'inventaire) est un fichier texte dans lequel nous spécifions les hôtes (machines) sur lesquels nous voulons exécuter vos tâches Ansible.
    mysql_playbook.yml: il s'agit du fichier de playbook Ansible destiné à la gestion de MySQL.
    wordpress_playbook.yml: il s'agit du fichier de playbook Ansible destiné au déploiement de l'application Wordpress.
    wordpress.conf.j2: il s'agit d'un modèle Jinja2 utilisé pour générer une configuration spécifique à WordPress pour un serveur web (par exemple, Apache ou Nginx) lors de la gestion de configurations avec Ansible.
- Fichiers à la racine:
    deploy.sh: il s'agit du script d'automatisation du déploiement de notre infrastructure.


#### Description des pré-requis:
- Système Linux.
- Compte GCP


###### Déploiement de l'application sur le Cloud provider Google à l'aide de Terraform:
Après s'être créé un compte GCP, il va falloir récupérer la clé du compte de service au format json que l'on pourras trouver dans sa console GCP dans la section IAM.
Renommez cette clé credentials.json au besoin, et la placer dans son dossier terraform.
Elle contient les informations d'authentification au compte GCP rattaché (Attention à ne pas l'envoyer sur un repos git publique ! ).

Renseigner les variables d'environnement disponibles sur cette clé dans le fichier variables.tf

Le fichier main.tf va faire référence au cloud provider utilisé.
Nous allons ensuite déployer deux instances :
Une pour Wordpress et une pour sa base de données au sein d'un même VPC qui va contenir un sous-réseau afin que les machines puissent communiquer entre elles.
Il va également falloir établir une règle de par feu et ouvrir les ports de communication.
Ainsi après obtention d'une adresse ip externe, ces machines seront accessibles depuis l'extérieur.
Afin de communiquer en ssh dessus, nous ouvrons également le port 22.
Il faut générer une pair de clé privée et publique afin de pouvoir se connecter en ssh avec la commande ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
Les clés se trouveront dans le dossier '~/.ssh/id_rsa'
Ensuite utiliser la clé publique pour se connecter.
La clé privée restant sur notre machine hôte.
Attention de ne pas envoyer la clé sur notre dépôt distant !
Nous pouvons donc l'inclure dans notre fichier .gitignore
Le protocole icmp permet de pouvoir pinger les machines entre elles.
Les output sont les valeurs de sortie que l'on souhaite obtenir après le déploiement de votre infrastructure.
Il est important à la création de nos instances de bien définir une machine disponible dans une zone et une région précise, sinon le déploiement ne fonctionnera pas et sortira une erreur.


###### Commandes Terraform à executer:
Une fois notre infrastructure mise en place, nous allons exécuter au sein du dossier Terraform les commandes suivantes :
- terraform init: cette commande est utilisée pour initialiser un répertoire Terraform en téléchargeant les fournisseurs de ressources spécifiés dans votre configuration Terraform.
- terraform plan: cette commande est utilisée pour créer un plan d'exécution. Elle examine votre configuration Terraform, évalue l'état actuel de l'infrastructure et compare ces informations pour déterminer quelles actions Terraform doit prendre pour atteindre l'état souhaité. Le plan est affiché dans la sortie, mais aucune modification n'est effectuée à ce stade.
- terraform apply: cette commande est utilisée pour appliquer le plan généré par terraform plan. Elle effectue les changements nécessaires pour aligner l'état de l'infrastructure avec la configuration Terraform spécifiée.

Si tout se passe correctement, l'infrastructure doit se déployer correctement.
Pour vérifier cela, nous pouvons nous connecter en ssh sur chacune de nos instances et les pinger entre elles.
Si la communication est bien établie, les datas seront transmises.


###### Provisionning de notre infrastructure avec Ansible:
Afin de pouvoir provisionner Terraform avec Ansible et de pouvoir déployer l'application WordPress ainsi que sa base de données, nous allons utiliser deux playbook.

Le wordpress_playbook.yml contient les informations pour récupérer le paquet WordPress au format tgz.
Il va extraire et copier les données dans le dossier var/www/html à l'aide du serveur apache.
Afin de vérifier que le serveur Apache2 fonctionne, nous pouvons utiliser la commande : sudo systemctl status apache2
Nous utilisons l'adresse ip externe de l'instance WordPress dans notre navigateur, pour vérifier que nous tombons bien sur la page du serveur Apache2.
Pour déployer WordPress, il va utiliser le fichier wordpress.conf.j2 pour envoyer les informations au serveur Apache dans le répertoire html.

Le mysql_playbook.yml contient les informations de la base de données nécessaire à Wordpress pour fonctionner.
Il va installer mariaDBServer et créer un nom de base de données, avec son utilisateur et mot de passe que l'on peut retrouver dans le fichier group_vars.yml.
Afin de vérifier son fonctionnement, nous pouvons nous connecter en ssh sur cette instance et utiliser une commande mysql sudo systemctl status mysql, puis mysql -u VOTRE_UTILISATEUR -p
Ensuite, il sera alors possible de se connecter à la base de données en effectuant un SHOW DATABASES;
Penser également à modifier les adresses ip externes des instances, et les indiquer dans le fichier hosts, sinon Ansible ne pourrat pas provisionner les machines.


###### Déploiement à l'aide du script d'automatisation:
Afin d'automatiser le déploiement de notre infrastructure et de vérifier que tout fonctionne correctement, nous pouvons nous placer à la racine de notre dossier et de lancer le script à l'aide de la commande ./deploy
Attention de bien bénéficier des droits nécessaires pour l'exécution du script avec la commande chmod +x mon_script.sh
Chaque étape du script est commentée.
Nous avons également inclus dans notre playbook WordPress les commandes commande: "ls -la /var/www/html" pour vérifier lors du déploiement du script que les dossiers sont bien provisionnés.
Cependant Ansible ne renvois aucun fichiers mais indiquerat une erreur si une étape ne fonctionne pas.
Il est également possible d'effectuer cette action manuellement en se connectant à l'instance WordPress et de vérifier si le dossier html est bien provisionner.

Pour termimer et se connecter au dashboard WordPress, nous pouvons récupérer l'adresse ip externe de l'instance wordpress et de la coller dans la navigateur, suivi de /wordpress et nous nous arriverons sur la page d'accueil d'installation de WordPress.
